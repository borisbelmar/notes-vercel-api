import { NowRequest, NowResponse } from '@vercel/node'
import { createTypeormConnection } from '../../../src/config/orm'
import { AuthController } from '../../../src/controllers'
import { UserEntity } from '../../../src/entities'
import { UserRepository } from '../../../src/repositories'
import closeConnectionIfExists from '../../../src/utils/typeorm/closeConnectionIfExists'
import { handleHttpError, onMethod } from '../../../src/utils/vercel'

export default async (req: NowRequest, res: NowResponse): Promise<void> => {
  try {
    const repository = new UserRepository(UserEntity)
    const controller = new AuthController(repository)
    await onMethod(req, res, {
      POST: async () => {
        await createTypeormConnection()
        const userLogin = req.body
        const payload = await controller.login(userLogin)
        res.status(200).send(payload)
      }
    })
  } catch (e) {
    handleHttpError(res, e)
  } finally {
    await closeConnectionIfExists()
  }
}
