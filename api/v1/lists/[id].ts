import { NowRequest, NowResponse } from '@vercel/node'
import { createTypeormConnection } from '../../../src/config/orm'
import { ListController } from '../../../src/controllers'
import { ListEntity } from '../../../src/entities'
import { ListRepository } from '../../../src/repositories'
import { tokenVerify } from '../../../src/utils/jwt'
import closeConnectionIfExists from '../../../src/utils/typeorm/closeConnectionIfExists'
import { handleHttpError, onMethod } from '../../../src/utils/vercel'

export default async (req: NowRequest, res: NowResponse): Promise<void> => {
  try {
    const user = tokenVerify(req.headers.authorization)
    const repository = new ListRepository(ListEntity, user)
    const controller = new ListController(repository)
    await onMethod(req, res, {
      GET: async () => {
        await createTypeormConnection()
        const { id } = req.query
        const numberId = parseInt(String(id), 10)
        const payload = await controller.getOneById(numberId)
        res.status(200).send(payload)
      },
      PUT: async () => {
        await createTypeormConnection()
        const { id } = req.query
        const numberId = parseInt(String(id), 10)
        const list = req.body
        await controller.update(numberId, list)
        res.status(204).end()
      },
      DELETE: async () => {
        await createTypeormConnection()
        const { id } = req.query
        const numberId = parseInt(String(id), 10)
        await controller.delete(numberId)
        res.status(204).end()
      }
    })
  } catch (e) {
    handleHttpError(res, e)
  } finally {
    await closeConnectionIfExists()
  }
}
