import { NowRequest, NowResponse } from '@vercel/node'
import { createTypeormConnection } from '../../../src/config/orm'
import { ListController } from '../../../src/controllers'
import { ListEntity } from '../../../src/entities'
import { ListRepository } from '../../../src/repositories'
import { tokenVerify } from '../../../src/utils/jwt'
import closeConnectionIfExists from '../../../src/utils/typeorm/closeConnectionIfExists'
import { handleHttpError, onMethod } from '../../../src/utils/vercel'

export default async (req: NowRequest, res: NowResponse): Promise<void> => {
  try {
    const user = tokenVerify(req.headers.authorization)
    const repository = new ListRepository(ListEntity, user)
    const controller = new ListController(repository)
    await onMethod(req, res, {
      GET: async () => {
        await createTypeormConnection()
        const payload = await controller.getAll()
        res.status(200).send(payload)
      },
      POST: async () => {
        await createTypeormConnection()
        const list = { ...req.body, user: user.id }
        const payload = await controller.create(list)
        res.status(201).send(payload)
      }
    })
  } catch (e) {
    handleHttpError(res, e)
  } finally {
    await closeConnectionIfExists()
  }
}
