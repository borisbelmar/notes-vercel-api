import { NowRequest, NowResponse } from '@vercel/node'
import { createTypeormConnection } from '../../../src/config/orm'
import { NoteController } from '../../../src/controllers'
import { NoteEntity } from '../../../src/entities'
import { NoteRepository } from '../../../src/repositories'
import { tokenVerify } from '../../../src/utils/jwt'
import closeConnectionIfExists from '../../../src/utils/typeorm/closeConnectionIfExists'
import { handleHttpError, onMethod } from '../../../src/utils/vercel'

export default async (req: NowRequest, res: NowResponse): Promise<void> => {
  try {
    const user = tokenVerify(req.headers.authorization)
    const repository = new NoteRepository(NoteEntity, user)
    const controller = new NoteController(repository)
    await onMethod(req, res, {
      GET: async () => {
        await createTypeormConnection()
        const payload = await controller.getAll()
        res.status(200).send(payload)
      },
      POST: async () => {
        await createTypeormConnection()
        const note = req.body
        const payload = await controller.create(note)
        res.status(201).send(payload)
      }
    })
  } catch (e) {
    handleHttpError(res, e)
  } finally {
    await closeConnectionIfExists()
  }
}
