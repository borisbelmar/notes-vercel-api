import { IList, INote, IUser } from '../dto'
import { IListRepository, INoteRepository, IUserRepository } from '../repositories/IRepository'
import { listMock, notesMock, usersMock } from './entities'

export const userRepositoryMock: jest.Mocked<IUserRepository> = {
  findAll: jest.fn().mockImplementation(async () => usersMock),
  findOne: jest.fn().mockImplementation(async id => usersMock.find(user => user.id === id)),
  create: jest.fn().mockImplementation(async () => 0),
  update: jest.fn().mockImplementation(async () => undefined),
  delete: jest.fn().mockImplementation(async () => undefined),
  findOneByEmail: jest.fn().mockImplementation(async email => usersMock.find(user => user.email === email))
}

export const listRepositoryMock: jest.Mocked<IListRepository> = {
  findAll: jest.fn().mockImplementation(async () => listMock),
  findOne: jest.fn().mockImplementation(async id => listMock.find(list => list.id === id)),
  create: jest.fn().mockImplementation(async () => 0),
  update: jest.fn().mockImplementation(async () => undefined),
  delete: jest.fn().mockImplementation(async () => undefined)
}

export const noteRepositoryMock: jest.Mocked<INoteRepository> = {
  findAll: jest.fn().mockImplementation(async () => notesMock),
  findOne: jest.fn().mockImplementation(async id => notesMock.find(note => note.id === id)),
  create: jest.fn().mockImplementation(async () => 0),
  update: jest.fn().mockImplementation(async () => undefined),
  delete: jest.fn().mockImplementation(async () => undefined)
}