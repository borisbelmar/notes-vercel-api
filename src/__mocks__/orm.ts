import entitiesMock from "./entities";

export function getMockRepository (type: 'ListEntity' | 'UserEntity' | 'NoteEntity') {
  return {
    find: jest.fn().mockReturnValue(entitiesMock[type]),
    findOne: jest.fn().mockReturnValue(entitiesMock[type][0]),
    insert: jest.fn().mockReturnValue(1),
    update: jest.fn(),
    delete: jest.fn(),
    createQueryBuilder: jest.fn().mockReturnThis(),
    addSelect: jest.fn().mockReturnThis(),
    leftJoinAndSelect: jest.fn().mockReturnThis(),
    leftJoin: jest.fn().mockReturnThis(),
    where: jest.fn().mockReturnThis(),
    andWhere: jest.fn().mockReturnThis(),
    getMany: jest.fn().mockReturnValue(entitiesMock[type]),
    getOne: jest.fn().mockReturnValue(entitiesMock[type][0])
  }
}
