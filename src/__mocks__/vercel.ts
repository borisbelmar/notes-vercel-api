export const resMock = {
  status: jest.fn().mockReturnThis(),
  send: jest.fn(),
  end: jest.fn()
}
