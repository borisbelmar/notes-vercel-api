import { IList, INote, IUser } from '../dto'

// All password are 12345
export const usersMock: IUser[] = [
  { id: 1, firstname: 'User1', lastname: 'User1', email: 'user1@test.com', password: '$2y$10$hDIa6sBJuto908FlQ9tTWet556s7ydhs5UFilCLvYLzqdhsPp/hBa' },
  { id: 2, firstname: 'User2', lastname: 'User2', email: 'user2@test.com', password: '$2y$10$NZrwxXv9RbAC0q4t3pNXbuKEv6SalefMfYgCzt6SQqb8VbQpBU6ZW' },
  { id: 3, firstname: 'User3', lastname: 'User3', email: 'user3@test.com', password: '$2y$10$ykPFloS637GIGltQxGQpkOA9/Ptm9BGp6sQyJ5HNw5xx4guebjxS.' }
]

export const listMock: IList[] = [
  { id: 1, title: 'List 1', slug: 'list-1', user: usersMock[0] },
  { id: 2, title: 'List 2', slug: 'list-2', user: usersMock[0] },
  { id: 3, title: 'List 3', slug: 'list-3', user: usersMock[0] }
]

export const notesMock: INote[] = [
  { id: 1, title: 'Note 1', content: 'This is the note 1', list: listMock[0] },
  { id: 2, title: 'Note 2', content: 'This is the note 2', list: listMock[1] },
  { id: 3, title: 'Note 3', content: 'This is the note 3', list: listMock[0] }
]

export default {
  ListEntity: listMock,
  UserEntity: usersMock,
  NoteEntity: notesMock
}
