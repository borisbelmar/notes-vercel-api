export interface IEntity {
  id?: number
  createdAt?: number
  updatedAt?: number
}
