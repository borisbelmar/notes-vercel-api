import { IEntity } from './IEntity'

export interface IUser extends IEntity {
  firstname: string
  lastname: string
  email: string
  password?: string
}

export interface IUserRegister extends IUser {
  password: string
}

export interface IUserLogin {
  email: string
  password: string
}

export interface IUserToken {
  token: string
}
