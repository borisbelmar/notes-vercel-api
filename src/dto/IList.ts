import { IEntity } from './IEntity'
import { INote } from './INote'
import { IUser } from './IUser'

export interface IList extends IEntity {
  title: string
  slug: string
  user?: IUser
  notes?: INote[]
}

export interface INewList {
  title: string
  slug: string
}
