import { IEntity } from './IEntity'
import { IList } from './IList'

export interface INote extends IEntity {
  title: string
  content?: string
  list?: IList
}

export interface INewNote {
  title: string
  content?: string
  list?: number
}
