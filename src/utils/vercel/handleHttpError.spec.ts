import { QueryFailedError } from "typeorm"
import HttpError from "../../errors/HttpError"
import { resMock } from "../../__mocks__/vercel"
import { handleHttpError } from "./handleHttpError"

describe('Test handleHttpError module', () => {

  beforeEach(() => {
    resMock.status.mockClear()
    resMock.send.mockClear()
  })

  it('Should send 500 http error with unknown error', () => {
    console.log = jest.fn()
    handleHttpError(resMock as any, new Error())
    expect(console.log).toBeCalled()
    expect(resMock.status).toHaveBeenCalledTimes(1)
    expect(resMock.status).toHaveBeenCalledWith(500)
    expect(resMock.send).toHaveBeenCalledTimes(1)
    expect(resMock.send).toHaveBeenCalledWith({ error: 'INTERNAL_SERVER_ERROR' })
  })

  it('Should send 404 http error', () => {
    handleHttpError(resMock as any, new HttpError(404))
    expect(resMock.status).toHaveBeenCalledTimes(1)
    expect(resMock.status).toHaveBeenCalledWith(404)
    expect(resMock.send).toHaveBeenCalledTimes(1)
    expect(resMock.send).toHaveBeenCalledWith({ error: 'NOT_FOUND' })
  })

  it('Should send 404 http error with custom message', () => {
    handleHttpError(resMock as any, new HttpError(404, 'CUSTOM_MESSAGE'))
    expect(resMock.status).toHaveBeenCalledTimes(1)
    expect(resMock.status).toHaveBeenCalledWith(404)
    expect(resMock.send).toHaveBeenCalledTimes(1)
    expect(resMock.send).toHaveBeenCalledWith({ error: 'CUSTOM_MESSAGE' })
  })

  it('Should send 500 http error with BD unknown error', () => {
    handleHttpError(resMock as any, new QueryFailedError('', [], 0))
    expect(resMock.status).toHaveBeenCalledTimes(1)
    expect(resMock.status).toHaveBeenCalledWith(500)
    expect(resMock.send).toHaveBeenCalledTimes(1)
    expect(resMock.send).toHaveBeenCalledWith({ error: 'UNKNOWN_DATABASE_ERROR' })
  })
  
  it('Should send correct http error with known BD error', () => {
    const errorMock = { constructor: QueryFailedError, code: 23505 }
    handleHttpError(resMock as any, errorMock as any)
    expect(resMock.status).toHaveBeenCalledTimes(1)
    expect(resMock.status).toHaveBeenCalledWith(409)
    expect(resMock.send).toHaveBeenCalledTimes(1)
    expect(resMock.send).toHaveBeenCalledWith({ error: 'DUPLICATED_RECORD' })
  })
})