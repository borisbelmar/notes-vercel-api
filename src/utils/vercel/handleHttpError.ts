import { NowResponse } from '@vercel/node'
import { QueryFailedError } from 'typeorm'
import HttpError from '../../errors/HttpError'
import { handlePostgresError } from '../typeorm/handlePostgresError'

export const handleHttpError = (
  res: NowResponse,
  e: HttpError | QueryFailedError | Error
) : void => {
  if (e.constructor === QueryFailedError) {
    const bdHttpError = handlePostgresError(e)
    res.status(bdHttpError[0] || 500).send({ error: bdHttpError[1] || 'UNKNOWN_DATABASE_ERROR' })
  } else {
    if (!(e as HttpError).status) {
      console.log(e)
    }
    res.status((e as HttpError).status || 500).send({ error: e.message || 'INTERNAL_SERVER_ERROR' })
  }
}
