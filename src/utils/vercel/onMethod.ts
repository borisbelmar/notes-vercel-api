import { NowRequest, NowResponse } from '@vercel/node'
import HttpError from '../../errors/HttpError'

type HttpCallback = (req: NowRequest, res: NowResponse) => Promise<void>
type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'OPTIONS'

export const onMethod = async (
  req: NowRequest,
  res: NowResponse,
  httpCallbacks: {
    GET?: HttpCallback,
    POST?: HttpCallback
    PUT?: HttpCallback
    DELETE?: HttpCallback
    OPTIONS?: HttpCallback
  }
): Promise<void> => {
  const allCallbacks = {
    OPTIONS: () => {
      res.status(100).end()
    },
    ...httpCallbacks
  }
  const callback = allCallbacks[req.method as HttpMethod]
  if (callback) {
    await callback(req, res)
  } else {
    throw new HttpError(405)
  }
}
