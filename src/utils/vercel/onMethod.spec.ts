import HttpError from '../../errors/HttpError'
import { resMock } from '../../__mocks__/vercel'
import { onMethod } from './onMethod'

describe('Testing vercel onMethod module', () => {
  it('Should execute the get callback', async done => {
    const getCallback = jest.fn()
    const reqMock: any = { method: 'GET' }
    await onMethod(
      reqMock,
      resMock as any,
      {
        GET: getCallback
      }
    )
    expect(getCallback).toHaveBeenCalledTimes(1)
    expect(getCallback).toHaveBeenCalledWith(reqMock, resMock)
    done()
  })

  it('Should respond with 405 status code', async done => {
    const reqMock: any = { method: 'GET' }
    try {
      await onMethod(reqMock, resMock as any, {})
    } catch (error) {
      expect(error).toBeInstanceOf(HttpError)
      expect(error.status).toBe(405)
      expect(error.message).toEqual('METHOD_NOT_ALLOWED')
    }
    done()
  })

  it('Should send status code 100 with OPTIONS method', async done => {
    const reqMock: any = { method: 'OPTIONS' }
    await onMethod(reqMock, resMock as any, {})
    expect(resMock.status).toHaveBeenCalledTimes(1)
    expect(resMock.status).toHaveBeenCalledWith(100)
    expect(resMock.end).toHaveBeenCalledTimes(1)
    done()
  })
})
