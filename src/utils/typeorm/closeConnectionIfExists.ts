import { getConnection } from 'typeorm'

const closeConnectionIfExists = async (): Promise<void> => {
  try {
    await getConnection().close()
  } catch {
    //
  }
}

export default closeConnectionIfExists
