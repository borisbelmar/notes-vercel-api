import closeConnectionIfExists from "./closeConnectionIfExists"
import { getConnection } from 'typeorm'

describe('Test closeConnectionIfExists module', () => {
  it('Should not fail', async done => {
    await closeConnectionIfExists()
    expect(getConnection().close).toHaveBeenCalledTimes(1)
    done()
  })
})