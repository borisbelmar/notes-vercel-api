import { QueryFailedError } from 'typeorm'
import { HttpErrorCode } from '../../errors/HttpError'

type PostgresErrorCode = 23505

type PostgresErrorHttp = [HttpErrorCode, string]

type PostgresErrorCodes = {
  [key: string]: PostgresErrorHttp
}

export const pgErrorCodes: PostgresErrorCodes = {
  23505: [409, 'DUPLICATED_RECORD']
}

export interface PostgresError extends QueryFailedError {
  code: PostgresErrorCode
}

export const handlePostgresError = (
  error: QueryFailedError | PostgresError
): PostgresErrorHttp => {
  const httpError: PostgresErrorHttp = pgErrorCodes[(error as PostgresError).code] || []
  if (!httpError.length) {
    console.log(error)
  }
  return httpError
}
