import { handlePostgresError, PostgresError, pgErrorCodes } from "./handlePostgresError"

describe('Test handlePostgresError module', () => {
  it('Should return duplicated error', () => {
    const mockError = { code: 23505 }
    const httpError = handlePostgresError(mockError as PostgresError)
    expect(httpError).toEqual(pgErrorCodes[23505])
  })
})