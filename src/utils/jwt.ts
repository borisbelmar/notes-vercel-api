import jwt from 'jsonwebtoken'
import { IUser } from '../dto'
import HttpError from '../errors/HttpError'

const PRIVATE_KEY = process.env.JWT_SECRET || 'supersecret'

export const tokenVerify = (bearerToken?: string): IUser => {
  try {
    if (bearerToken?.includes('Bearer')) {
      const token = bearerToken.split(' ')[1]
      return jwt.verify(token, PRIVATE_KEY) as IUser
    }
    throw Error()
  } catch (e) {
    throw new HttpError(403, 'INVALID_TOKEN')
  }
}
export const tokenSign = (user: IUser): string => {
  const tokenPayload = {
    sub: user.id,
    ...user
  }
  return jwt.sign(tokenPayload, PRIVATE_KEY, { expiresIn: '1d' })
}
