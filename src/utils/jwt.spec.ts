import { IUser } from '../dto'
import HttpError from '../errors/HttpError'
import { tokenSign, tokenVerify } from './jwt'

describe('Test jwt module', () => {
  const user: IUser = { id: 1, firstname: 'First', lastname: 'Last', email: 'test@test.com' }
  let token: string

  beforeEach(() => {
    token = tokenSign(user)
  })

  it('Should sign token correctly', () => {
    const tokenVerified = tokenVerify(`Bearer ${token}`)
    expect(tokenVerified.id).toBe(1)
  })

  it('Should fail if bearer not come', () => {
    try {
      tokenVerify(token)
    } catch (error) {
      expect(error).toBeInstanceOf(HttpError)
      expect(error.status).toBe(403)
      expect(error.message).toEqual('INVALID_TOKEN')
    }
  })

  it('Should fail if token is not well formed', () => {
    try {
      tokenVerify('Bearer badtoken')
    } catch (error) {
      expect(error).toBeInstanceOf(HttpError)
      expect(error.status).toBe(403)
      expect(error.message).toEqual('INVALID_TOKEN')
    }
  })

  it('Should fail if token not come', () => {
    try {
      tokenVerify()
    } catch (error) {
      expect(error).toBeInstanceOf(HttpError)
      expect(error.status).toBe(403)
      expect(error.message).toEqual('INVALID_TOKEN')
    }
  })
})
