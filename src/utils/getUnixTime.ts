function getUnixTime (timestamp?: number): number {
  const dateForUnixTime = timestamp || Date.now()
  return Math.floor(dateForUnixTime / 1000)
}

export default getUnixTime
