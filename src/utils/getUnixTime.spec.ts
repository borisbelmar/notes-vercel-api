import getUnixTime from './getUnixTime'

describe('Test getUnixTime module', () => {
  it('Get unix time correctly', () => {
    const timestamp = Date.now()
    expect(getUnixTime(timestamp)).toBe(Math.floor(timestamp / 1000))
  })

  it('Get unix time from Date.now if timestamp not setted', () => {
    const dateNowSpy = jest.spyOn(Date, 'now')
    getUnixTime()
    expect(dateNowSpy).toBeCalled()
  })
})
