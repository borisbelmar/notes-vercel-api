import { BeforeInsert, BeforeUpdate, Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm'
import { IsOptional, Length } from 'class-validator'
import { Type } from 'class-transformer'
import slugify from 'slugify'

import { IList } from '../dto'
import EntityBase from './EntityBase'
import NoteEntity from './NoteEntity'
import UserEntity from './UserEntity'

@Entity({ name: 'list' })
export default class ListEntity extends EntityBase implements IList {
  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  title!: string

  @OneToMany(() => NoteEntity, note => note.list)
  @IsOptional()
  @JoinColumn()
  notes?: NoteEntity[]

  @ManyToOne(() => UserEntity, { nullable: false })
  @Type(() => UserEntity)
  @JoinColumn()
  user!: UserEntity

  @Column({ length: 50, nullable: false, unique: true })
  @Length(3, 50)
  @IsOptional()
  slug!: string

  @BeforeInsert()
  @BeforeUpdate()
  public slugify (): void {
    if (!this.slug) {
      this.slug = slugify(this.title, { lower: true })
    }
  }
}
