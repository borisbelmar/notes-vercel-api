import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm'
import { Length } from 'class-validator'
import { Type } from 'class-transformer'

import EntityBase from './EntityBase'
import { INote } from '../dto'
import ListEntity from './ListEntity'

@Entity({ name: 'note' })
export default class NoteEntity extends EntityBase implements INote {
  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  title!: string

  @Column({ type: 'text', nullable: true })
  content!: string

  @ManyToOne(() => ListEntity, list => list.notes, { nullable: false })
  @Type(() => ListEntity)
  @JoinColumn()
  list!: ListEntity
}
