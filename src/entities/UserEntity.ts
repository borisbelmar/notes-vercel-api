import { BeforeInsert, BeforeUpdate, Column, Entity } from 'typeorm'
import { IsEmail, Length } from 'class-validator'
import bcrypt from 'bcryptjs'

import EntityBase from './EntityBase'
import { IUser } from '../dto'

@Entity({ name: 'user' })
export default class UserEntity extends EntityBase implements IUser {
  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  firstname!: string

  @Column({ length: 50, nullable: false })
  @Length(3, 50)
  lastname!: string

  @Column({ length: 80, nullable: false, unique: true })
  @Length(3, 80)
  @IsEmail()
  email!: string

  @Column({ length: 255, nullable: false, select: false })
  @Length(3, 255)
  password?: string

  @BeforeInsert()
  @BeforeUpdate()
  public hashPassword (): void {
    if (this.password) {
      this.password = bcrypt.hashSync(this.password, 10)
    }
  }
}
