import { Column, BeforeUpdate, BeforeInsert, PrimaryGeneratedColumn } from 'typeorm'
import { IsInt, IsOptional } from 'class-validator'
import getUnixTime from '../utils/getUnixTime'

export default abstract class EntityBase implements EntityBase {
  @PrimaryGeneratedColumn()
  @IsOptional()
  id?: number

  @Column({ type: 'int' })
  @IsOptional()
  createdAt?: number

  @Column({ type: 'int' })
  @IsInt()
  @IsOptional()
  updatedAt?: number

  @BeforeInsert()
  public setCreatedAt (): void {
    if (!this.createdAt) {
      this.createdAt = getUnixTime()
    }
    if (!this.updatedAt) {
      this.updatedAt = getUnixTime()
    }
  }

  @BeforeUpdate()
  public setUpdatedAt (): void {
    if (!this.updatedAt) {
      this.updatedAt = getUnixTime()
    }
  }
}
