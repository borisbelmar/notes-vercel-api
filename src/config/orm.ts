import { Connection, createConnection } from 'typeorm'
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'
import { ListEntity, NoteEntity, UserEntity } from '../entities'

const config: PostgresConnectionOptions = {
  type: 'postgres',
  host: process.env.TYPEORM_HOST || 'localhost',
  port: process.env.TYPEORM_PORT || 5432,
  username: process.env.TYPEORM_USERNAME || 'postgres',
  password: process.env.TYPEORM_PASSWORD || 'password',
  database: process.env.TYPEORM_DATABASE || 'postgres',
  connectTimeoutMS: 5000,
  synchronize: false,
  logging: false,
  entities: [
    UserEntity,
    NoteEntity,
    ListEntity
  ]
}

export const createTypeormConnection = async (): Promise<Connection> => createConnection(config)
