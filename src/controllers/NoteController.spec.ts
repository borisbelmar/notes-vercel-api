import { validate } from 'class-validator'
import { NoteController } from '.'
import HttpError from '../errors/HttpError'
import { notesMock } from '../__mocks__/entities'
import { noteRepositoryMock } from '../__mocks__/repositories'

describe('Test ListController class', () => {
  const repository = noteRepositoryMock
  const mockedValidate = validate as any as jest.Mock<typeof validate>
  let controller: NoteController

  beforeAll(() => {
    controller = new NoteController(repository)
  })

  it('Should get all notes correctly', async done => {
    const response = await controller.getAll()
    expect(repository.findAll).toBeCalled()
    expect(response).toEqual(notesMock)
    done()
  })

  it('Should get and item from notes if exists', async done => {
    const response = await controller.getOneById(1)
    expect(repository.findOne).toBeCalled()
    expect(response).toEqual(notesMock.find(item => item.id === 1))
    done()
  })

  it('Should throw error 404 if note not exists', async done => {
    try {
      await controller.getOneById(10)
    } catch (e) {
      expect(repository.findOne).toBeCalled()
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(404)
    }
    done()
  })

  it('Should create new note', async done => {
    const newRecord = { title: 'New list', list: 1 }
    const response = await controller.create(newRecord)
    expect(response).toEqual({ id: 0, ...newRecord })
    expect(noteRepositoryMock.create).toBeCalled()
    expect(noteRepositoryMock.create).toBeCalledWith(newRecord)
    done()
  })

  it('Should update a note', async done => {
    const updateRecord = { title: 'New list2', slug: 'new-list2' }
    const response = await controller.update(0, updateRecord)
    expect(response).toEqual(undefined)
    expect(noteRepositoryMock.update).toBeCalled()
    expect(noteRepositoryMock.update).toBeCalledWith(0, updateRecord)
    done()
  })

  it('Should delete a note', async done => {
    const response = await controller.delete(0)
    expect(response).toEqual(undefined)
    expect(noteRepositoryMock.delete).toBeCalled()
    expect(noteRepositoryMock.delete).toBeCalledWith(0)
    done()
  })

  it('Should create fails if validating gives errors', async done => {
    mockedValidate.mockReturnValueOnce([1, 2] as any)
    console.log = jest.fn()
    try {
      await controller.create({ title: 'New list', list: 1 })
      expect(true).toBe(false)
    } catch(e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(400)
    }
    done()
  })
})
