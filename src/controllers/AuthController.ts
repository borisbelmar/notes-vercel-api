import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import bcrypt from 'bcryptjs'
import { omit } from 'ramda'
import { IUserLogin, IUserRegister, IUserToken } from '../dto'
import UserEntity from '../entities/UserEntity'
import HttpError from '../errors/HttpError'
import { tokenSign } from '../utils/jwt'
import { IUserRepository } from '../repositories/IRepository'

export default class AuthController {
  private repository: IUserRepository

  constructor (repository: IUserRepository) {
    this.repository = repository
    this.login = this.login.bind(this)
    this.register = this.register.bind(this)
  }

  public async login (userData: IUserLogin): Promise<IUserToken> {
    const data = await this.repository.findOneByEmail(userData.email)

    if (data?.password && bcrypt.compareSync(userData.password, data.password)) {
      const token = tokenSign(omit(['password'], data))
      return { token }
    }
    throw new HttpError(403, 'INVALID_CREDENTIALS')
  }

  public async register (userRegister: IUserRegister): Promise<void> {
    const newUserEntity = plainToClass(UserEntity, userRegister)

    const errors = await validate(newUserEntity)

    if (errors.length) {
      console.log(errors)
      throw new HttpError(400)
    }

    await this.repository.create(newUserEntity)
  }
}
