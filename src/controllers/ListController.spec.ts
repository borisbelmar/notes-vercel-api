import { validate } from 'class-validator'
import { ListController } from '.'
import HttpError from '../errors/HttpError'
import { listMock } from '../__mocks__/entities'
import { listRepositoryMock } from '../__mocks__/repositories'

describe('Test ListController class', () => {

  const repository = listRepositoryMock
  const mockedValidate = validate as any as jest.Mock<typeof validate>
  let controller: ListController

  beforeAll(() => {
    controller = new ListController(repository)
  })

  it('Should get all list correctly', async done => {
    const response = await controller.getAll()
    expect(repository.findAll).toBeCalled()
    expect(response).toEqual(listMock)
    done()
  })

  it('Should get and item from lists if exists', async done => {
    const response = await controller.getOneById(1)
    expect(repository.findOne).toBeCalled()
    expect(response).toEqual(listMock.find(list => list.id === 1))
    done()
  })

  it('Should throw error 404 if list not exists', async done => {
    try {
      await controller.getOneById(10)
    } catch (e) {
      expect(repository.findOne).toBeCalled()
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(404)
    }
    done()
  })

  it('Should create new list', async done => {
    const newRecord = { title: 'New list', slug: 'new-list' }
    const response = await controller.create(newRecord)
    expect(response).toEqual({ id: 0, ...newRecord })
    expect(listRepositoryMock.create).toBeCalled()
    expect(listRepositoryMock.create).toBeCalledWith(newRecord)
    done()
  })

  it('Should update a list', async done => {
    const updateRecord = { title: 'New list2', slug: 'new-list2' }
    const response = await controller.update(0, updateRecord)
    expect(response).toEqual(undefined)
    expect(listRepositoryMock.update).toBeCalled()
    expect(listRepositoryMock.update).toBeCalledWith(0, updateRecord)
    done()
  })

  it('Should delete a list', async done => {
    const response = await controller.delete(0)
    expect(response).toEqual(undefined)
    expect(listRepositoryMock.delete).toBeCalled()
    expect(listRepositoryMock.delete).toBeCalledWith(0)
    done()
  })

  it('Should create fails if validating gives errors', async done => {
    mockedValidate.mockReturnValueOnce([1, 2] as any)
    console.log = jest.fn()
    try {
      await controller.create(listMock[0])
      expect(true).toBe(false)
    } catch(e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(400)
    }
    done()
  })
})
