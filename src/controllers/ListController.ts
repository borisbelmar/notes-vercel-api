import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import HttpError from '../errors/HttpError'
import ListEntity from '../entities/ListEntity'
import { IList } from '../dto'
import { IListRepository } from '../repositories/IRepository'

export default class ListController {
  private repository: IListRepository

  constructor (repository: IListRepository) {
    this.repository = repository
    this.getAll = this.getAll.bind(this)
    this.getOneById = this.getOneById.bind(this)
    this.create = this.create.bind(this)
    this.update = this.update.bind(this)
    this.delete = this.delete.bind(this)
  }

  public async getAll (): Promise<IList[]> {
    return this.repository.findAll()
  }

  public async getOneById (id: number): Promise<IList> {
    const data = await this.repository.findOne(id)
    if (data) {
      return data
    }
    throw new HttpError(404)
  }

  public async create (list: IList): Promise<IList> {
    const newListEntity = plainToClass(ListEntity, list)

    const errors = await validate(newListEntity)

    if (errors.length > 0) {
      console.log(errors)
      throw new HttpError(400)
    }

    const insertedId = await this.repository.create(newListEntity)
    return { id: insertedId, ...list }
  }

  public async update (id: number, list: IList): Promise<void> {
    const listEntity = plainToClass(ListEntity, list)
    await this.repository.update(id, listEntity)
  }

  public async delete (id: number): Promise<void> {
    await this.repository.delete(id)
  }
}
