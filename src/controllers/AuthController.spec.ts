import { validate } from 'class-validator'
import jwt from 'jsonwebtoken'
import { AuthController } from '.'
import { IUserLogin, IUserRegister } from '../dto'
import HttpError from '../errors/HttpError'
import { usersMock } from '../__mocks__/entities'
import { userRepositoryMock } from '../__mocks__/repositories'

describe('Test AuthController class', () => {
  const mockedValidate = validate as any as jest.Mock<typeof validate>
  let controller: AuthController

  beforeAll(() => {
    controller = new AuthController(userRepositoryMock)
  })

  it('Should login correctly', async done => {
    const mockCredentials: IUserLogin = { email: usersMock[0].email, password: '12345' }
    const response = await controller.login(mockCredentials)
    const decodedToken = jwt.decode(response.token)
    expect(userRepositoryMock.findOneByEmail).toBeCalled()
    expect(userRepositoryMock.findOneByEmail).toBeCalledWith(usersMock[0].email)
    expect(decodedToken).toHaveProperty('id', usersMock[0].id)
    expect(decodedToken).not.toHaveProperty('password')
    done()
  })

  it('Should fail login with invalid credentials', async done => {
    const invalidCredentials1: IUserLogin = { email: 'nonExistent@email.com', password: '12345' }
    try {
      await controller.login(invalidCredentials1)
      expect(true).toBe(false)
    } catch (error) {
      expect(error).toBeInstanceOf(HttpError)
      expect(error).toHaveProperty('status', 403)
      expect(error).toHaveProperty('message', 'INVALID_CREDENTIALS')
    }
    
    const invalidCredentials2: IUserLogin = { email: usersMock[0].email, password: '123456' }
    try {
      await controller.login(invalidCredentials2)
      expect(true).toBe(false)
    } catch (error) {
      expect(error).toBeInstanceOf(HttpError)
      expect(error).toHaveProperty('status', 403)
      expect(error).toHaveProperty('message', 'INVALID_CREDENTIALS')
    }
    done()
  })

  it('Should register correctly', async done => {
    const mockCredentials: IUserRegister = { ...usersMock[0], password: '12345' }
    await controller.register(mockCredentials)
    expect(userRepositoryMock.create).toBeCalled()
    expect(userRepositoryMock.create).toBeCalledWith(mockCredentials)
    done()
  })

  it('Should register fails if validating gives errors', async done => {
    const mockCredentials: IUserRegister = { ...usersMock[0], password: '12345' }
    mockedValidate.mockReturnValueOnce([1, 2] as any)
    console.log = jest.fn()
    try {
      await controller.register(mockCredentials)
      expect(true).toBe(false)
    } catch(e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(400)
    }
    done()
  })
})
