import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import HttpError from '../errors/HttpError'
import NoteEntity from '../entities/NoteEntity'
import { INote } from '../dto'
import { INoteRepository } from '../repositories/IRepository'
import { INewNote } from '../dto/INote'

export default class NoteController {
  private repository: INoteRepository

  constructor (repository: INoteRepository) {
    this.repository = repository
    this.getAll = this.getAll.bind(this)
    this.getOneById = this.getOneById.bind(this)
    this.create = this.create.bind(this)
    this.update = this.update.bind(this)
    this.delete = this.delete.bind(this)
  }

  public async getAll (): Promise<INote[]> {
    const data = await this.repository.findAll()
    return data
  }

  public async getOneById (id: number): Promise<INote> {
    const numberId = parseInt(String(id), 10)
    const data = await this.repository.findOne(numberId)
    if (data) {
      return data
    }
    throw new HttpError(404)
  }

  public async create (note: INewNote): Promise<INote> {
    const newNoteEntity = plainToClass(NoteEntity, note)

    const errors = await validate(newNoteEntity)

    if (errors.length > 0) {
      console.log(errors)
      throw new HttpError(400)
    }

    const insertedId = await this.repository.create(newNoteEntity)
    return { id: insertedId, ...newNoteEntity }
  }

  public async update (id: number, note: INote): Promise<void> {
    const listEntity = plainToClass(NoteEntity, note)

    await this.repository.update(id, listEntity)
  }

  public async delete (id: number): Promise<void> {
    await this.repository.delete(id)
  }
}
