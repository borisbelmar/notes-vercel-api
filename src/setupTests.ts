// Libs mocks

jest.mock('typeorm', () => ({
  getConnection: jest.fn().mockReturnThis(),
  close: jest.fn(),
  create: jest.fn(),
  getRepository: jest.fn(),
  PrimaryGeneratedColumn: jest.fn(),
  Column: jest.fn(),
  BeforeInsert: jest.fn(),
  BeforeUpdate: jest.fn(),
  Entity: jest.fn(),
  ManyToOne: jest.fn(),
  JoinColumn: jest.fn(),
  OneToMany: jest.fn(),
  QueryFailedError: jest.fn()
}))

jest.mock('class-validator', () => ({
  Length: jest.fn(),
  IsOptional: jest.fn(),
  IsInt: jest.fn(),
  IsEmail: jest.fn(),
  validate: jest.fn().mockReturnValue([])
}))

jest.mock('class-transformer', () => ({
  Type: jest.fn(),
  plainToClass: jest.fn().mockImplementation((_, item) => item)
}))
