import { getRepository } from 'typeorm'
import { usersMock } from '../__mocks__/entities'
import { getMockRepository } from '../__mocks__/orm'
import UserRepository from './UserRepository'

const mockedGetRepository = getRepository as jest.MockedFunction<typeof getRepository>
const mockedRepository = getMockRepository('UserEntity')
mockedGetRepository.mockImplementation(() => mockedRepository as any)

describe('Test ListRepository class', () => {
  let repository: UserRepository

  beforeEach(() => {
    repository = new UserRepository({} as any) 
  })

  it('Should call repository find method', async done => {
    const response = await repository.findAll()
    expect(response).toBe(usersMock)
    expect(mockedRepository.find).toBeCalled()
    expect(mockedRepository.find).toBeCalledWith()
    expect(mockedRepository.find).toReturnWith(usersMock)
    done()
  })

  it('Should call repository findOne method', async done => {
    const response = await repository.findOne(1)
    expect(response).toBe(usersMock[0])
    expect(mockedRepository.findOne).toBeCalled()
    expect(mockedRepository.findOne).toBeCalledWith(1)
    expect(mockedRepository.findOne).toReturnWith(usersMock[0])
    done()
  })

  it('Should call repository create method', async done => {
    await repository.create(usersMock[0])
    expect(mockedRepository.insert).toBeCalled()
    expect(mockedRepository.insert).toBeCalledWith(usersMock[0])
    expect(mockedRepository.insert).toReturnWith(1)
    done()
  })

  it('Should call repository update method', async done => {
    await repository.update(1, usersMock[0])
    expect(mockedRepository.update).toBeCalled()
    expect(mockedRepository.update).toBeCalledWith(1, usersMock[0])
    expect(mockedRepository.update).toReturnWith(undefined)
    done()
  })

  it('Should call repository delete method', async done => {
    await repository.delete(1)
    expect(mockedRepository.delete).toBeCalled()
    expect(mockedRepository.delete).toBeCalledWith({ id: 1 })
    expect(mockedRepository.delete).toReturnWith(undefined)
    done()
  })

  it('Should call repository findOneByEmail method', async done => {
    const email = 'test@test.com'
    const response = await repository.findOneByEmail(email)
    expect(response).toBe(usersMock[0])
    expect(mockedRepository.addSelect).toBeCalledWith('user.password')
    expect(mockedRepository.where).toBeCalledWith('user.email = :email', { email })
    expect(mockedRepository.getOne).toBeCalled()
    done()
  })
})
