import { path } from 'ramda'
import { EntityTarget, getConnection } from 'typeorm'
import { IUser } from '../dto'
import { UserEntity } from '../entities'
import { IUserRepository } from './IRepository'

export default class UserRepository implements IUserRepository {
  private entity: EntityTarget<UserEntity>

  constructor (entity: EntityTarget<UserEntity>) {
    this.entity = entity
  }

  public findAll = (): Promise<IUser[]> => (
    getConnection()
      .getRepository(this.entity)
      .find()
  )

  public findOne = (id: number): Promise<IUser | undefined> => (
    getConnection()
      .getRepository(this.entity)
      .findOne(id)
  )

  public create = async (user: IUser): Promise<number> => {
    const response = await getConnection().getRepository(this.entity).insert(user)
    return path(['identifiers', 0, 'id'], response) || 0
  }

  public update = async (id: number, user: IUser): Promise<void> => {
    await getConnection().getRepository(this.entity).update(id, user)
  }

  public delete = async (id: number): Promise<void> => {
    await getConnection().getRepository(this.entity).delete({ id })
  }

  public findOneByEmail = (email: string): Promise<IUser | undefined> => (
    getConnection()
      .getRepository(this.entity)
      .createQueryBuilder('user')
      .addSelect('user.password')
      .where('user.email = :email', { email })
      .getOne()
  )
}
