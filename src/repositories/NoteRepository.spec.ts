import { getRepository } from 'typeorm'
import HttpError from '../errors/HttpError'
import { listMock, notesMock, usersMock } from '../__mocks__/entities'
import { getMockRepository } from '../__mocks__/orm'
import NoteRepository from './NoteRepository'

const mockedGetRepository = getRepository as jest.MockedFunction<typeof getRepository>
const mockedRepository = getMockRepository('NoteEntity')
mockedGetRepository.mockImplementation(() => mockedRepository as any)

describe('Test ListRepository class', () => {
  const user = usersMock[0]
  let repository: NoteRepository

  beforeEach(() => {
    repository = new NoteRepository({} as any, user) 
  })

  it('Should call repository find method', async done => {
    await repository.findAll()
    expect(mockedRepository.leftJoinAndSelect).toBeCalledWith('notes.list', 'list')
    expect(mockedRepository.where).toBeCalled()
    expect(mockedRepository.where).toBeCalledWith('list.user = :uid', { uid: 1 })
    expect(mockedRepository.getMany).toBeCalled()
    expect(mockedRepository.getMany).toReturnWith(notesMock)
    done()
  })

  it('Should call repository findOne method', async done => {
    await repository.findOne(1)
    expect(mockedRepository.leftJoinAndSelect).toBeCalledWith('notes.list', 'list')
    expect(mockedRepository.where).toBeCalled()
    expect(mockedRepository.where).toBeCalledWith('note.id = :id', { id: 1 })
    expect(mockedRepository.andWhere).toBeCalledWith('list.user = :uid', { uid: 1 })
    expect(mockedRepository.getOne).toBeCalled()
    expect(mockedRepository.getOne).toReturnWith(notesMock[0])
    done()
  })

  it('Should call repository create method', async done => {
    mockedRepository.findOne.mockReturnValueOnce(listMock[0])
    await repository.create(notesMock[0])
    expect(mockedRepository.insert).toBeCalled()
    expect(mockedRepository.insert).toBeCalledWith(notesMock[0])
    expect(mockedRepository.insert).toReturnWith(1)
    done()
  })

  it('Should call repository update method', async done => {
    mockedRepository.findOne.mockReturnValueOnce(listMock[0])
    await repository.update(1, notesMock[0])
    expect(mockedRepository.update).toBeCalled()
    expect(mockedRepository.update).toBeCalledWith({ id: 1 }, notesMock[0])
    expect(mockedRepository.update).toReturnWith(undefined)
    done()
  })

  it('Should call repository delete method', async done => {
    mockedRepository.findOne.mockReturnValueOnce(notesMock[0])
    await repository.delete(1)
    expect(mockedRepository.delete).toBeCalled()
    expect(mockedRepository.delete).toBeCalledWith({ id: 1 })
    expect(mockedRepository.delete).toReturnWith(undefined)
    done()
  })

  it('Should throw error 403 if user is incorrect', async done => {
    const repositoryWithoutPermission = new NoteRepository({} as any, { ...user, id: 10 }) 
    mockedRepository.findOne.mockReturnValueOnce(listMock[0])
    try {
      await repositoryWithoutPermission.create(notesMock[0])
      expect(true).toBe(false)
    } catch (e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(403)
    }
    mockedRepository.findOne.mockReturnValueOnce(listMock[0])
    try {
      await repositoryWithoutPermission.update(1, notesMock[0])
      expect(true).toBe(false)
    } catch (e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(403)
    }
    mockedRepository.findOne.mockReturnValueOnce(notesMock[0])
    try {
      await repositoryWithoutPermission.delete(1)
      expect(true).toBe(false)
    } catch (e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(403)
    }
    done()
  })

  it('Should throw error 404 if item to delete doesnt exists', async done => {
    const repositoryWithoutPermission = new NoteRepository({} as any, user) 
    mockedRepository.findOne.mockReturnValueOnce(undefined)
    try {
      await repositoryWithoutPermission.delete(1)
      expect(true).toBe(false)
    } catch (e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(404)
    }
    done()
  })

  it('Should throw error 400 if list not exists on create', async done => {
    mockedRepository.findOne.mockReturnValueOnce(undefined)
    try {
      await repository.create(notesMock[0])
      expect(true).toBe(false)
    } catch (e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(400)
    }
    done()
  })

  it('Should throw error 400 if list not exists on update', async done => {
    mockedRepository.findOne.mockReturnValueOnce(undefined)
    try {
      await repository.update(1, notesMock[0])
      expect(true).toBe(false)
    } catch (e) {
      expect(e).toBeInstanceOf(HttpError)
      expect(e.status).toBe(400)
    }
    done()
  })
})
