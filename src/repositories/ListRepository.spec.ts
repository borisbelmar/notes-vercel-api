import { getRepository } from 'typeorm'
import { listMock, usersMock } from '../__mocks__/entities'
import { getMockRepository } from '../__mocks__/orm'
import ListRepository from './ListRepository'

const mockedGetRepository = getRepository as jest.MockedFunction<typeof getRepository>
const mockedRepository = getMockRepository('ListEntity')
mockedGetRepository.mockImplementation(() => mockedRepository as any)

describe('Test ListRepository class', () => {
  const user = usersMock[0]
  let repository: ListRepository

  beforeEach(() => {
    repository = new ListRepository({} as any, user) 
  })

  it('Should call repository find method', async done => {
    await repository.findAll()
    expect(mockedRepository.find).toBeCalled()
    expect(mockedRepository.find).toBeCalledWith({ relations: ['user'], where: { user: 1 } })
    expect(mockedRepository.find).toReturnWith(listMock)
    done()
  })

  it('Should call repository findOne method', async done => {
    await repository.findOne(1)
    expect(mockedRepository.findOne).toBeCalled()
    expect(mockedRepository.findOne).toBeCalledWith(1, { relations: ['user'], where: { user: 1 } })
    expect(mockedRepository.findOne).toReturnWith(listMock[0])
    done()
  })

  it('Should call repository create method', async done => {
    await repository.create(listMock[0])
    expect(mockedRepository.insert).toBeCalled()
    expect(mockedRepository.insert).toBeCalledWith(listMock[0])
    expect(mockedRepository.insert).toReturnWith(1)
    done()
  })

  it('Should call repository update method', async done => {
    await repository.update(1, listMock[0])
    expect(mockedRepository.update).toBeCalled()
    expect(mockedRepository.update).toBeCalledWith({ id: 1, user: { id: user.id }}, listMock[0])
    expect(mockedRepository.update).toReturnWith(undefined)
    done()
  })

  it('Should call repository delete method', async done => {
    await repository.delete(1)
    expect(mockedRepository.delete).toBeCalled()
    expect(mockedRepository.delete).toBeCalledWith({ id: 1, user: { id: user.id }})
    expect(mockedRepository.delete).toReturnWith(undefined)
    done()
  })
})
