/* eslint-disable @typescript-eslint/no-empty-interface */
import { IList, INote, IUser } from '../dto'

export interface IRepository<Tdto, Tid> {
  findAll: () => Promise<Tdto[]>
  findOne: (id: Tid) => Promise<Tdto | undefined>
  create: (entity: Tdto) => Promise<Tid>
  update: (id: Tid, entity: Tdto) => Promise<void>
  delete: (id: Tid) => Promise<void>
}

export interface IUserRepository extends IRepository<IUser, number> {
  findOneByEmail: (email: string) => Promise<IUser | undefined>
}

export interface IListRepository extends IRepository<IList, number> {}

export interface INoteRepository extends IRepository<INote, number> {}
