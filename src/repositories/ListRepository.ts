import { path } from 'ramda'
import { EntityTarget, getConnection } from 'typeorm'
import { IList, IUser } from '../dto'
import { ListEntity } from '../entities'
import { IListRepository } from './IRepository'

export default class ListRepository implements IListRepository {
  private entity: EntityTarget<ListEntity>

  private user: IUser

  constructor (entity: EntityTarget<ListEntity>, user: IUser) {
    this.entity = entity
    this.user = user
  }

  public findAll = (): Promise<IList[]> => (
    getConnection()
      .getRepository(this.entity)
      .find({ relations: ['user'], where: { user: this.user.id } })
  )

  public findOne = (id: number): Promise<IList | undefined> => (
    getConnection()
      .getRepository(this.entity)
      .findOne(id, { relations: ['user'], where: { user: this.user.id } })
  )

  public create = async (list: IList): Promise<number> => {
    const response = await getConnection().getRepository(this.entity).insert(list)
    return path(['identifiers', 0, 'id'], response) || 0
  }

  public update = async (id: number, list: IList): Promise<void> => {
    await getConnection()
      .getRepository(this.entity)
      .update({ id, user: { id: this.user.id } }, list)
  }

  public delete = async (id: number): Promise<void> => {
    await getConnection()
      .getRepository(this.entity)
      .delete({ id, user: { id: this.user.id } })
  }
}
