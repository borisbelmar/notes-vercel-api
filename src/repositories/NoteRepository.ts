import { path } from 'ramda'
import { EntityTarget, getConnection } from 'typeorm'
import { IList, INote, IUser } from '../dto'
import { NoteEntity } from '../entities'
import HttpError from '../errors/HttpError'
import { INoteRepository } from './IRepository'

export default class NoteRepository implements INoteRepository {
  private entity: EntityTarget<NoteEntity>

  private user: IUser

  constructor (entity: EntityTarget<NoteEntity>, user: IUser) {
    this.user = user
    this.entity = entity
  }

  public findAll = (): Promise<INote[]> => (
    getConnection()
      .getRepository(this.entity)
      .createQueryBuilder('notes')
      .leftJoinAndSelect('notes.list', 'list')
      .where('list.user = :uid', { uid: this.user.id })
      .getMany()
  )

  public findOne = (id: number): Promise<INote | undefined> => (
    getConnection()
      .getRepository(this.entity)
      .createQueryBuilder('note')
      .leftJoinAndSelect('note.list', 'list')
      .where('note.id = :id', { id })
      .andWhere('list.user = :uid', { uid: this.user.id })
      .getOne()
  )

  public create = async (note: INote): Promise<number> => {
    const connection = getConnection()
    const list = await connection.getRepository('list').findOne(note.list, { relations: ['user'] }) as IList
    if (list && list.user) {
      if (list.user.id === this.user.id) {
        const response = await connection.getRepository(this.entity).insert(note)
        return path(['identifiers', 0, 'id'], response) || 0
      }
      throw new HttpError(403, 'NOT_LIST_PERMISSION')
    } else {
      throw new HttpError(400, 'LIST_NOT_EXISTS')
    }
  }

  public update = async (id: number, note: INote): Promise<void> => {
    const connection = getConnection()
    const list = await connection.getRepository('list').findOne(note.list, { relations: ['user'] }) as IList
    if (list && list.user) {
      if (list.user.id === this.user.id) {
        await connection.getRepository(this.entity).update({ id }, note)
      } else {
        throw new HttpError(403, 'NOT_LIST_PERMISSION')
      }
    } else {
      throw new HttpError(400, 'LIST_NOT_EXISTS')
    }
  }

  public delete = async (id: number): Promise<void> => {
    const connection = getConnection()
    const note = await connection.getRepository(this.entity).findOne(id, { relations: ['list', 'list.user'] }) as INote
    if (note) {
      if (note.list?.user?.id === this.user.id) {
        await connection.getRepository(this.entity).delete({ id })
      } else {
        throw new HttpError(403, 'NOT_LIST_PERMISSION')
      }
    } else {
      throw new HttpError(404)
    }
  }
}
