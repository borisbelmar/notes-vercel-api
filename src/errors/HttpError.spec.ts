import HttpError, { defaultErrorMessages } from './HttpError'

describe('Testing HttpError method module', () => {
  it('Empty constructor must be correct', () => {
    const error = new HttpError()
    expect(error.status).toBe(500)
    expect(error.message).toBe(defaultErrorMessages[500])
  })

  it('Error with code only must throw properly message', () => {
    const code = 400
    const error = new HttpError(code)
    expect(error.status).toBe(code)
    expect(error.message).toBe(defaultErrorMessages[code])
  })

  it('Error with code and custom message', () => {
    const code = 400
    const message = 'CUSTOM'
    const error = new HttpError(code, message)
    expect(error.status).toBe(code)
    expect(error.message).toBe(message)
  })
})