export type HttpErrorCode = 400 | 401 | 403 | 404 | 405 | 409 | 500

export const defaultErrorMessages = {
  400: 'BAD_REQUEST',
  401: 'UNAUTHORIZED',
  403: 'FORBIDDEN',
  404: 'NOT_FOUND',
  405: 'METHOD_NOT_ALLOWED',
  409: 'CONFLICT',
  500: 'INTERNAL_SERVER_ERROR'
}

export default class HttpError extends Error {
  public status

  constructor (status: HttpErrorCode = 500, message?: string) {
    super(message || defaultErrorMessages[status])
    this.status = status
  }
}
