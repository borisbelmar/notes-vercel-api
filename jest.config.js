module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFiles: [
    '<rootDir>/src/setupTests.ts'
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/entities/*'
  ]
}
