module.exports = {
  type: 'postgres',
  host: process.env.TYPEORM_HOST || 'localhost',
  port: process.env.TYPEORM_PORT || 5432,
  username: process.env.TYPEORM_USERNAME || 'postgres',
  password: process.env.TYPEORM_PASSWORD || 'password',
  database: process.env.TYPEORM_DATABASE || 'postgres',
  connectTimeoutMS: 5000,
  synchronize: true,
  logging: false,
  entities: [
    './src/entities/*.ts'
  ],
  migrations: [
    './src/migrations/*.ts'
  ],
  cli: {
    entitiesDir: 'src/entities/',
    migrationsDir: 'src/migrations'
  }
}
